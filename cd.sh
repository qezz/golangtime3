#!/bin/bash

TIME=$(date -u +%Y-%m-%dT%H:%M:%SZ)

gsed -i -E "s@..___PLACEHOLDER___.+@/*___PLACEHOLDER___*/ currentTime := \"$TIME\""@ lib.go
git add lib.go
git commit -m "Update time: $TIME"
git push origin master
